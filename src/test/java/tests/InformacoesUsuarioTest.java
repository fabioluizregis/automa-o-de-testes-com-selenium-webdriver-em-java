package tests;


import org.easetech.easytest.annotation.DataLoader;
import org.easetech.easytest.annotation.Param;
import org.easetech.easytest.runner.DataDrivenTestRunner;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import suporte.Generator;
import suporte.Screenshot;
import suporte.Web;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

@RunWith(DataDrivenTestRunner.class)
@DataLoader(filePaths = "../automa-o-de-testes-com-selenium-webdriver-em-java/src/test/java/resources/InformacoesUsuarioTestData.csv")

public class InformacoesUsuarioTest {
    private WebDriver navegador;

    @Rule
    public TestName test = new TestName();

    @Before
    public void setup(){
        navegador = Web.createChrome();

        //Clicar no link que possui o texto "Sign in"
        WebElement linkSignIn = navegador.findElement(By.linkText("Sign in"));
        linkSignIn.click();

        //Digitar no campo com o name "login" que está dentro do formulário de id "signinbox" o texto "Julio0001"
        WebElement formularioSignInBox = navegador.findElement(By.id("signinbox"));
        formularioSignInBox.findElement(By.name("login")).sendKeys("Julio0001");

        //Digitar no campo com o nome "password" que estã dentro do formulário de id "signinbox" o texto "123456"
        formularioSignInBox.findElement(By.name("password")).sendKeys("123456");

        //Clicar no link com o texto "SIGN IN"
        formularioSignInBox.findElement(By.linkText("SIGN IN")).click();

        //Validar que dentro do elemento com class "me" está o texto "Hi, Julio"
//        WebElement me = navegador.findElement(By.className("me"));
//        String textoNoElementoMe = me.getText();
//        assertEquals("Hi, Julio", textoNoElementoMe);

        //Clicar em um link que possui a class me
        navegador.findElement(By.className("me")).click();

        //Clicar em um link que possui o texto "MORE DATA ABOUT YOU"
        navegador.findElement(By.linkText("MORE DATA ABOUT YOU")).click();

    }
//        navegador.manage().window().maximize();
//        navegador.manage().window().setSize(new Dimension(1280, 800));

    @Test
    public void testAdicionarUmaInformacaoAdicionalDoUsuario0001(@Param(name="tipo")String tipo, @Param(name="contato")String contato, @Param(name="mensagem")String mensagemEsperada){
        //Clicar no botão "add more data" através do seu xpath //button[@data-target="addmoredata"]
        navegador.findElement(By.xpath("//button[@data-target=\"addmoredata\"]")).click();

        //Identificar a popup onde está o formulário de id addmoredata
        WebElement popupAddMoreData = navegador.findElement(By.id("addmoredata"));

        //Na combo de name "type" escoler a opção Phone
        WebElement campoType = popupAddMoreData.findElement(By.name("type"));
//        new Select(campoType).selectByVisibleText("Phone");
        new Select(campoType).selectByVisibleText(tipo);

        //No campo de name "contact" digitar "+5599999999"
//        popupAddMoreData.findElement(By.name("contact")).sendKeys("+5599999999");
        popupAddMoreData.findElement(By.name("contact")).sendKeys(contato);

        //Clicar no link de text "SAVE" que está na popup
        popupAddMoreData.findElement(By.linkText("SAVE")).click();

        //Na mensagem de id "toast-container" validar que o texto é "Your contact has been added!"
        WebElement mensagemPop = navegador.findElement(By.id("toast-container"));
        String mensagem = mensagemPop.getText();
//        assertEquals("Your contact has been added!", mensagem);
        assertEquals(mensagemEsperada, mensagem);
    }

    @Test
    public void removerUmContatoDeUmUsuario(){
        //remover +551133334444  //span[text()='11999998888']/following-sibling::a
        navegador.findElement(By.xpath("//span[text()=\"+5599999999\"]/following-sibling::a")).click();

        //confirmar a janela de javaScript
        navegador.switchTo().alert().accept();

        //Validar que a mensagem apresentada foi Rest in peace, dear phone!
        WebElement mensagemPop = navegador.findElement(By.id("toast-container"));
        String mensagem = mensagemPop.getText();
        assertEquals("Rest in peace, dear phone!", mensagem);

        //Pegar evidência do teste
        Screenshot.tirar(navegador, "../automa-o-de-testes-com-selenium-webdriver-em-java/src/test/java/evidencias/" + Generator.dataHoraParaArquivo() + test.getMethodName() + ".png");

        //Aguardar até 10 segundos para que a janela desapareça
        WebDriverWait aguardar = new WebDriverWait(navegador, 10);
        aguardar.until(ExpectedConditions.stalenessOf(mensagemPop));

        //Clicar no link com o texto "Logout"
        navegador.findElement(By.linkText("Logout")).click();

    }

    @After
    public void tearDown(){
        //Fechar o navegador
        navegador.close();
    }
}
