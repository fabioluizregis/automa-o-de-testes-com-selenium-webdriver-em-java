package tests;

import org.easetech.easytest.annotation.DataLoader;
import org.easetech.easytest.annotation.Param;
import org.easetech.easytest.runner.DataDrivenTestRunner;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import pages.LoginPage;
import suporte.Web;

import static org.junit.Assert.*;

@RunWith(DataDrivenTestRunner.class)
@DataLoader(filePaths = "../automa-o-de-testes-com-selenium-webdriver-em-java/src/test/java/resources/testAdicionarUmaInformacaoAdicionalDoUsuario.csv")


//para rodar por linha de comando. Instalar o maven, colocar o diretório no Path e executar o comando abaixo:
// mvn clean test -Dtest=tests.InformacoesUsuarioPageObjectsTest
public class InformacoesUsuarioPageObjectsTest {
    private WebDriver navegador;

    @Before
    public void setup(){
        navegador = Web.createChrome();
//        navegador = Web.createBrowserStack();
    }

    @Test
    public void testAdicionarUmaInformacaoAdicionalDoUsuario(@Param(name="tipo")String tipo,
                                                             @Param(name="contato")String contato,
                                                             @Param(name="mensagem")String mensagem){
        String textoToast = new LoginPage(navegador)
                .clickSignIn()
                .fazerLogin("julio0001","123456")
                .clicarMe()
                .clicarAbaMoreDataAboutYou()
                .clicarNoBotaoAddMoreDataAboutYou()
                .adicionarContato(tipo,contato)
                .capturarTextoToast();

        assertEquals(mensagem, textoToast);
    }

    @After
    public void tearDown(){
        navegador.quit();
    }
}
